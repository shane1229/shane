﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if(ModelState.IsValid)
            {
                var mh = data.h.Value / 100;
                var R = data.w.Value / (mh * mh);
               
                data.R = R;
                var l = "";
                if (R < 18.5) {
                    l = "體重過輕";
                }else if(R >=18.5&& R < 24)
                {
                    l= "正常範圍";
                }else if (R >= 24 && R < 27)
                {
                   l = "過重";
                }else if (R >= 27 && R < 30)
                {
                    l = "輕度肥胖";
                }else if (R >= 30 && R < 35)
                {
                   l= "中度肥胖" ;
                }else if (R > 35)
                {
                    l = "重度肥胖";
                }
                data.l = l;
                
            }
            return View(data);
        }
    }
}