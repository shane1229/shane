﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class BMIData
    {
        [Display(Name = "身高")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(100, 300, ErrorMessage = "請輸入100-300的數值")]

        public float? h { get; set; }
        [Display(Name = "體重")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(30, 150, ErrorMessage = "請輸入30-150的數值")]

        public float? w { get; set; }
        public float? R { get; set; }
        public String l { get; set; }


    }
}